/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package ops

import (
	"com.mgface.disobj/apinode/api"
	"com.mgface.disobj/apinode/objstream"
)

// objName 对象名称
// index 数据分片
func putStream(hashValue, objName string, index int, expectIps []string) (*objstream.PutStream, []string, error) {
	nodeAddr, expectIps, err := api.ChooseRandomDataNode(index, expectIps)
	if err != nil {
		return nil, nil, err
	}
	putstream := objstream.NewPutStream(hashValue, nodeAddr, objName, index)
	return putstream, expectIps, nil
}
