/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package ops

import (
	. "com.mgface.disobj/apinode/api"
	"com.mgface.disobj/apinode/objstream"
	"fmt"
	"io"
)

// 获得数据流
func getstream(object string) ([]io.Reader, []error, int64) {
	//定位数据分片
	//todo 如果定位不到，说明可能metadatanode挂了，那么直接根据缓存的数据连接datanode
	nodeAddrs, objRealNames, datasize := Locate(object, 3)
	errors := make([]error, DataShards)
	//返回的节点数组应该是是数据分片+奇偶校验分片数量
	totalsizes := DataShards + ParityShards
	if len(nodeAddrs) == 0 || len(nodeAddrs) != totalsizes {
		errors := append(errors, fmt.Errorf("obj:%s 定位失败", object))
		return nil, errors, 0
	}
	//我们只需数据分片大小的数据,不需要奇偶校验分片数据
	reader := make([]io.Reader, DataShards)
	//todo 这里需要考虑到查询数据的时候，数据丢失，需要使用SR纠删码进行数据修复
	//todo 并且考虑到get操作可能没有那么及时，所以我们需要在后台进行定期数据轮训做数据修复
	for index := range nodeAddrs[:DataShards] {
		reader[index], errors[index] = objstream.NewGetStream(nodeAddrs[index], objRealNames[index])
	}
	return reader, errors, datasize

}
