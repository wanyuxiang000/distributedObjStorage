/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package ops

import (
	"bytes"
	. "com.mgface.disobj/common"
	"compress/gzip"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

func Get(respo http.ResponseWriter, r *http.Request) {
	object := strings.Split(r.URL.EscapedPath(), "/")[2]
	streams, errors, datasize := getstream(object)

	for _, v := range errors {
		if v != nil {
			log.Warn("###发生错误:", v)
			respo.WriteHeader(http.StatusInternalServerError)
			respo.Write([]byte(v.Error()))
			return
		}
	}
	resp := make([]byte, 0)
	for _, data := range streams {
		data, e := ioutil.ReadAll(data)
		if e != nil {
			log.Warn("读取数据发生失败:", e.Error())
			respo.WriteHeader(http.StatusInternalServerError)
			respo.Write([]byte(e.Error()))
		}
		//进行gzip解压缩
		decodeData, _ := GzipDecode(data)
		resp = append(resp, decodeData...)
	}

	reader := bytes.NewReader(resp[:datasize])
	log.Debug("数据压缩识别.")
	acceptGzip := false
	encoding := r.Header["Accept-Encoding"]
	for i := range encoding {
		if encoding[i] == "gzip" {
			acceptGzip = true
			break
		}
	}
	if acceptGzip {
		respo.Header().Set("content-encoding", "gzip")
		gzipWriter := gzip.NewWriter(respo)
		io.Copy(gzipWriter, reader)
		gzipWriter.Close()
	} else {
		io.Copy(respo, reader)
	}
}
