/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package main

import (
	. "com.mgface.disobj/apinode/command"
	"com.mgface.disobj/common"
	"github.com/urfave/cli"
)

const usage = `
 *
 *   API服务提供对象数据的对外服务
 *
`

func main() {
	//执行的终端命令
	commands := []cli.Command{
		ApiStartCommand,
	}
	common.RunFn(usage, commands)
}
