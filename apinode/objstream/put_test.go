/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package objstream

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func putHandler(w http.ResponseWriter, r *http.Request) {
	b, _ := ioutil.ReadAll(r.Body)
	if string(b) == "test" {
		return
	}
	w.WriteHeader(http.StatusForbidden)
}

func TestPut(t *testing.T) {
	s := httptest.NewServer(http.HandlerFunc(putHandler))
	defer s.Close()

	ps := NewPutStream(s.URL[7:], "any", "", 0)
	io.WriteString(ps, "test")
	e := ps.Close()
	if e != nil {
		t.Error(e)
	}
}
