/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package handler

import (
	"com.mgface.disobj/apinode/api"
	"com.mgface.disobj/apinode/ops"
	"encoding/json"
	"net/http"
	"strings"
)

// ApiHandler 存储客户端提交的数据
//
// 主要识别GET方法和PUT方法
func ApiHandler(writer http.ResponseWriter, req *http.Request) {
	method := req.Method
	if method == http.MethodPut {
		ops.Put(writer, req)
		return
	}
	if method == http.MethodGet {
		ops.Get(writer, req)
		return
	}
	writer.WriteHeader(http.StatusMethodNotAllowed)
}

// LocateHandler 定位数据存储在哪个客户端
func LocateHandler(w http.ResponseWriter, req *http.Request) {
	m := req.Method
	if m != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	nodeAddr, _, _ := api.Locate(strings.Split(req.URL.EscapedPath(), "/")[2], 3)
	if len(nodeAddr) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Set("Content-type", "application/json")
	b, _ := json.Marshal(nodeAddr)
	w.Write(b)
}
