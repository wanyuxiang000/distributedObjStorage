/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package server

import (
	. "com.mgface.disobj/apinode/api"
	. "com.mgface.disobj/apinode/handler"
	. "com.mgface.disobj/common"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func StartServer(na, mna, podnamespace string) {
	log.Debug("启动API节点...")
	log.Debug(fmt.Sprintf("节点地址:%s", na))
	log.Debug(fmt.Sprintf("元数据服务节点地址:%s", mna))
	//创建2个启动标志，一个用来启动发送心跳服务，一个用来更新数据节点数据
	flags := make(chan bool, 2)
	//更新dynamicMetanodeAddr
	go RefreshMetaNodeAddr(mna, podnamespace, flags)
	//发送心跳包
	go StartApiHeartbeat(na, flags)
	//刷新数据节点数据
	go RefreshDNData(flags)
	http.HandleFunc("/objects/", ApiHandler)
	http.HandleFunc("/locate/", LocateHandler)

	ServeAndGracefulExit(na)
}
