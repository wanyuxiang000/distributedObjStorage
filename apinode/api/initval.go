/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

// DataShards 数据分片大小数量
var DataShards = 2

// ParityShards 奇偶校验数量
var ParityShards = 1

// InitProps 初始化配置信息
func InitProps(dataShards, parityShards int) {
	DataShards = dataShards
	ParityShards = parityShards
}
