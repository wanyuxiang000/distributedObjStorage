/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import (
	. "com.mgface.disobj/common"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"time"
)

// 刷新datanode值，每1秒更新一次
func RefreshDNData(startflag chan bool) {
	log.Info("获得启动标识2:", <-startflag)
	client := NewReCallFuncTCPClient(GetDynamicMetanodeAddr, 3)
	cmd := &Cmd{Name: "get", Key: "dataNodes", Value: "dataNodes"}
	//是否需要读取缓存的数据
	enabledCached := false

	//定义打印计数器
	printCount := 0
	for {
	reconn:
		if enabledCached {
			//重连
			log.Info(fmt.Sprintf("[更新内存datanode]重连元数据服务端[%s]...", GetDynamicMetanodeAddr()))
			client = NewReCallFuncTCPClient(GetDynamicMetanodeAddr, 1)
			if client != nil {
				enabledCached = false
				goto reconn
			}
		} else {
			//执行命令，获取dataNodes数据
			cmd.Value = "dataNodes"
			if client != nil {
				log.Debug("刷新datanodes的数据,dynamicMetaNode节点为:", GetDynamicMetanodeAddr())
				cmd.Run(client)
				if cmd.Error != nil {
					//如果连接不上元服务器端，那么直接取缓存的数据
					enabledCached = true
				} else {
					SetDataNodes("dataNodes", cmd.Value)
					enabledCached = false
				}
			} else {
				enabledCached = true
			}
		}
		if printCount > 5 {
			if enabledCached {
				dns := GetDataNodes("dataNodes")
				data, _ := json.MarshalIndent(dns, "", "\t")
				log.Info("获取【缓存】datanodes节点信息:", string(data))
			} else {
				data, _ := json.MarshalIndent(cmd.Value, "", "\t")
				log.Info("获取【实时】datanodes服务节点信息:", string(data))
			}
			printCount = 0
		}
		time.Sleep(1 * time.Second)
		printCount++
	}
}
