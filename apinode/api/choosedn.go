/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

//该文件主要是用来选择后台服务器节点

package api

import (
	. "com.mgface.disobj/common"
	"encoding/json"
	"errors"
	"math/rand"
	"time"
)

// 获取datanode节点数据
func getdanos() []string {
	dn := make([]string, 0)
	var metavalues []MetaValue
	json.Unmarshal([]byte(GetDataNodes("dataNodes")), &metavalues)
	for _, v := range metavalues {
		dn = append(dn, v.RealNodeValue)
	}
	return dn
}

// ChooseRandomDataNode 选择请求的随机数据节点
func ChooseRandomDataNode(index int, expectIps []string) (string, []string, error) {
	//设置随机种子，确保每次随机产生的数都是需要的数据
	rand.Seed(time.Now().UnixNano())
	dn := getdanos()
	n := len(dn)
	if n == 0 {
		return "", nil, errors.New("没有找到数据节点")
	}
	if n < 3 {
		data, _ := json.Marshal(&ReturnMsg{Msg: "数据节点必须>=3个", Flag: false})
		return "", nil, errors.New(string(data))
	}
	flag := true
	var data string
sureIp:
	for flag {
		//随机从当前的数据节点集里面取一个节点
		ranNodeAddr := dn[rand.Intn(n)]
		for _, v := range expectIps {
			//如果随机出来的数据节点已经存在了，重新随机
			if ranNodeAddr == v {
				flag = true
				goto sureIp
			}
		}
		//随机出来的数据节点之前没有使用，那么选择数据节点成功，推出循环
		flag = false
		data = ranNodeAddr
	}
	expectIps = append(expectIps, data)
	return expectIps[index], expectIps, nil
}
