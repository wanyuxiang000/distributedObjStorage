/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import (
	. "com.mgface.disobj/common"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"sort"
	"strings"
)

// 定位数据分片
//
// nodeAddr 节点的数组数据
//
// objRealName 对象数据文件名称
//
// filesize 文件大小
func Locate(object string, maxFailCount int) (nodeAddr []string, objRealName []string, filesize int64) {

	client := NewReCallFuncTCPClient(GetDynamicMetanodeAddr, maxFailCount)
	if client == nil {
		log.Warn("元数据节点定位失败.")
		return
	}

	cmd := &Cmd{Name: "get", Key: "metadata", Value: object}
	cmd.Run(client)
	var results []Datadigest
	json.Unmarshal([]byte(cmd.Value), &results)
	var max int64 = 0
	var maxDatadigest Datadigest
	for _, data := range results {
		if data.Version > max {
			max = data.Version
			maxDatadigest = data
		}
	}
	//获取文件的CRC
	cmd = &Cmd{Name: "get", Key: "filecrc", Value: maxDatadigest.Hash}
	cmd.Run(client)

	var sd SharedDataslice
	json.Unmarshal([]byte(cmd.Value), &sd)
	//对数据切片进行排序,因为前面最小的Index为实际的数据分片,后面的索引为CRC分片,用来做数据还原的
	sort.Stable(sd)
	nodeAddrs := make([]string, 0)
	objRealNames := make([]string, 0)
	//遍历文件CRC，获取请求数据的数据节点，实际文件名，分片大小
	for _, data := range sd {
		url := strings.Split(data.SharedFileUrlLocate, string(os.PathSeparator))
		//URL构造为:节点地址/文件存储的根路径/文件存储的日期/对象的实际存储名称
		nodeAddrs = append(nodeAddrs, url[0])
		objRealName := fmt.Sprintf("%s/%s", url[len(url)-2], url[len(url)-1])
		log.Debug("objRealName:::", objRealName)
		objRealNames = append(objRealNames, objRealName)
	}
	return nodeAddrs, objRealNames, maxDatadigest.Datasize
}

type SharedDataslice []SharedData

// 数据分片切片长度
func (s SharedDataslice) Len() int {
	return len(s)
}

// 数据分片的数据交换
func (s SharedDataslice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// 数据分片的比较
func (s SharedDataslice) Less(i, j int) bool {
	return s[i].SharedIndex < s[j].SharedIndex
}
