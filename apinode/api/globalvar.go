/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import "sync"

// 用来协调心跳服务和刷新服务
var syncRun sync.Once

// 全局锁
var apiNodeMutex sync.RWMutex

// 全局数据节点map集合，key为节点的addr，value是节点的更新时间
var dataNodes = make(map[string]string)

func SetDataNodes(key, value string) {
	apiNodeMutex.Lock()
	dataNodes[key] = value
	apiNodeMutex.Unlock()
}

func GetDataNodes(key string) string {
	apiNodeMutex.RLock()
	memdata := dataNodes[key]
	apiNodeMutex.RUnlock()
	return memdata
}

// 因为dynamicMetanodeAddr是全局变量。为了保证可见性，使用时需要使用apiMutex全局锁
var dynamicMetanodeAddr string

// 获取dynamicmetanode地址
func GetDynamicMetanodeAddr() string {
	apiNodeMutex.RLock()
	memdata := dynamicMetanodeAddr
	apiNodeMutex.RUnlock()
	return memdata

}

// 设置dynamicmetanode地址
func SetDynamicMetanodeAddr(nodeAddr string) {
	apiNodeMutex.Lock()
	dynamicMetanodeAddr = nodeAddr
	apiNodeMutex.Unlock()
}
