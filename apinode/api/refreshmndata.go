/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import (
	. "com.mgface.disobj/common"
	"com.mgface.disobj/common/k8s"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"sort"
	"strings"
	"time"
)

// RefreshMetaNodeAddr 动态更新RefreshMetaNodeAddr值，每500毫秒更新一次
func RefreshMetaNodeAddr(metaNodeAddr, podNamespace string, flag chan bool) {
	if podNamespace != "" {
		val := strings.Split(metaNodeAddr, ":")
		metaNodes := k8s.DetectMetaService(val[1], podNamespace, val[0])
		if len(metaNodes) > 0 {
			//随机选一个metaNode
			metaNodeAddr = metaNodes[0]
		}
	}

	//该metaNodeAddr作为种子节点取元数据
	client := NewReconTCPClient(metaNodeAddr, 3)
	if client == nil {
		log.Fatal(fmt.Sprintf("元数据服务连接失败,请提供一个正确的元数据服务IP地址"))
		os.Exit(-1)
	}

	//第一次查询提供元数据服务的信息
	cmd := &Cmd{Name: "get", Key: "metaNodes", Value: ""}

	//为了得到master节点信息
	cmd.Run(client)
	var results []MetaValue
	json.Unmarshal([]byte(cmd.Value), &results)
	for _, metavale := range results {
		//当key为metaNodes，RealNodeValue值为:"nodeaddr-(master|slave)"
		data := strings.Split(metavale.RealNodeValue, "-")
		if data[1] == "master" {
			SetDynamicMetanodeAddr(data[0])
			syncRun.Do(func() {
				flag <- true
				flag <- true
				close(flag)
			})
		}
	}
	//定义打印计数器
	printCount := 0
	for {
		sort.Stable(WrapMetaValues(results))
		//以第一次master返回的数据作为种子节点
		rss, _ := json.MarshalIndent(results, "", "\t")

		if printCount > 20 {
			fmt.Println("更新时间:", time.Now().Format("2006-01-02 15:04:05"), ",metaNodes数据::", string(rss))
			printCount = 0
		}

		//查询出来的数据
		srearchdata := make([]MetaValue, 0)

	gotit:
		for _, metavale := range results {
			log.Debug("当前查询的节点::", metavale.RealNodeValue)
			//查询提供元数据服务的信息
			cmd := &Cmd{Name: "get", Key: "metaNodes", Value: "metaNodes"}
			data := strings.Split(metavale.RealNodeValue, "-")
			livenodeAddr := data[0]
			//client = NewReconTCPClient(livenodeAddr, 3)
			client = NewTCPClient(livenodeAddr)
			//如果创建失败，说明该节点响应不了
			if client == nil {
				continue
			}
			//获取元服务节点
			cmd.Run(client)
			//如果出错也跳过当前遍历的节点，直接到下一个节点
			if cmd.Error != nil {
				continue
			} else {
				var rss []MetaValue
				json.Unmarshal([]byte(cmd.Value), &rss)
				for _, metavale := range rss {
					//当key为metaNodes，RealNodeValue值为:"nodeaddr-(master|slave)"
					data := strings.Split(metavale.RealNodeValue, "-")
					if data[1] == "master" {
						SetDynamicMetanodeAddr(data[0])
						syncRun.Do(func() {
							flag <- true
							flag <- true
							close(flag)
						})
						//如果是master的话直接赋值给results，让其遍历最新获取的数据
						srearchdata = rss
						break gotit
					}
				}

				//如果返回的数据为空，说明当前节点还没有选择出master(处于不一致状态)，可以暂停100ms，重新让下一个节点取获取
				if GetDynamicMetanodeAddr() == "" {
					time.Sleep(100 * time.Millisecond)
					continue
				}
			}
		}
		//假如查询出来数据，赋值
		if len(srearchdata) > 0 {
			results = srearchdata
		} else {
			log.Warn("没有更新到dynamicMetanodeAddr.")
		}
		//间隔500毫秒刷新一次dynamicMetanodeAddr
		time.Sleep(500 * time.Millisecond)
		printCount++
	}
}
