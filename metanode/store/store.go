/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package store

import (
	"bufio"
	. "com.mgface.disobj/metanode/core/nodeinfo"
)

type Store interface {
	// Set 设置数据值
	Set(key string, value []byte) error
	// Get 获取数据值
	Get(key string, value []byte) (interface{}, error)
	// Del 删除数据值
	Del(key string, value []byte) error
	// Put 保存文件数据
	Put(fname string, fszie int, reader *bufio.Reader) (interface{}, error)
	// Sync 同步请求
	Sync(synrlen int, reader *bufio.Reader) (interface{}, error)
	// Show 显示数据值
	Show() string
	// ExpireData 过期后台数据
	ExpireData(info *NodeInfo, intervalCleanMem int)
	// ThisToJson 把数据本身转成Json
	ThisToJson() string
	// AssignThis 对自身赋值操作
	AssignThis(data interface{})
}
