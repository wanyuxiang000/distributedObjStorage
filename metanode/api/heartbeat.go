/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import (
	. "com.mgface.disobj/common"
	"com.mgface.disobj/metanode/core/handler"
	"fmt"
	log "github.com/sirupsen/logrus"
	"time"
)

// StartMDeartbeat metadata节点也需要注册，供Apinode和datanode使用
//
// nodeAddr 节点当前的值
//
// nodeflag  节点当前是master还是slave
func StartMDeartbeat(nodeAddr string, serv *handler.Server, startflag chan bool) {
	log.Info("获得启动标识:", <-startflag)
restart:
	for {
		client := NewReCallFuncTCPClient(GetDynamicMNAddr, 3)
		if client == nil {
			log.Warn("metanode心跳包服务连接master节点失败，等待重连......")
			goto restart
		}
		//发送心跳包操作
		log.Debug("当前执行的master节点为:", GetDynamicMNAddr())
		metanodeInfo := fmt.Sprintf("%s-%s", nodeAddr, serv.Nodeinfo.GetNodeInfo())
		cmd := &Cmd{Name: "set", Key: "metaNodes", Value: metanodeInfo}
		cmd.Run(client)
		if cmd.Error != nil {
			log.Warn(fmt.Sprintf("%s,metanode心跳包服务发送心跳失败.", time.Now().Format("2006-01-02 15:04:05")))
		}
		time.Sleep(3 * time.Second)
	}
}
