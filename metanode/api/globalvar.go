/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import "sync"

// 全局锁
var metaNodeMutex sync.RWMutex

// 因为dynamicMNAddr是全局变量。为了保证可见性，使用时需要使用apiMutex全局锁
var dynamicMNAddr string

// GetDynamicMNAddr 获取dynamicmetanode地址
func GetDynamicMNAddr() string {
	metaNodeMutex.RLock()
	memdata := dynamicMNAddr
	metaNodeMutex.RUnlock()
	return memdata

}

// SetDynamicMNAddr 设置dynamicmetanode地址
func SetDynamicMNAddr(nodeAddr string) {
	metaNodeMutex.Lock()
	dynamicMNAddr = nodeAddr
	metaNodeMutex.Unlock()
}
