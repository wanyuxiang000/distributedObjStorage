/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package ops

import (
	"bufio"
	. "com.mgface.disobj/metanode/store"
)

// Get abnf规则]G<key.len> <key>
func Get(store Store, reader *bufio.Reader) (interface{}, error) {
	key, value, _ := readKeyAndValue(reader)
	return store.Get(key, value)
}

func Set(store Store, reader *bufio.Reader) error {
	key, value, e := readKeyAndValue(reader)
	store.Set(key, value)
	return e
}

func Del(store Store, reader *bufio.Reader) error {
	key, value, e := readKeyAndValue(reader)
	store.Del(key, value)
	return e
}

// Put 文件上传保持接口
func Put(store Store, reader *bufio.Reader) (interface{}, error) {
	fname, _ := readString(reader)

	fsize, _ := readLen(reader)
	return store.Put(fname, fsize, reader)
}

func Syn(store Store, reader *bufio.Reader) (interface{}, error) {
	synrlen, _ := readLen(reader)
	return store.Sync(synrlen, reader)
}
