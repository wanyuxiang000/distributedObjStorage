/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package file

import (
	. "com.mgface.disobj/common"
	. "com.mgface.disobj/metanode/core/memory"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
	"strings"
)

// LoadsnapshotData 加载内存数据快照到文件系统中
func LoadsnapshotData(cache *MemoryStore) {
	storepath := cache.StorePath
	//目录不存在直接跳过加载
	if _, err := os.Stat(storepath); os.IsNotExist(err) {
		return
	}
	fpaths := readLogfile(storepath)
	for _, fpath := range fpaths {
		//跳过同步给其他节点的文件
		if strings.Contains(fpath, ".sync") {
			continue
		}
		log.Info(fmt.Sprintf("加载快照文件:%s", fpath))
		f, _ := os.OpenFile(fpath, os.O_RDONLY, 0644)
		for {
			msgsize := make([]byte, 4)
			_, e := f.Read(msgsize)
			if e == io.EOF {
				break
			}
			size := BytesToInt(msgsize)
			ev := make([]byte, size)
			f.Read(ev)
			var msgs []RecMsg
			json.Unmarshal(ev, &msgs)
			for _, v := range msgs {
				cache.Set(v.Key, []byte(v.Val.(string)))
			}
		}
		f.Close()
	}
	cache.LoadingSnapshot = false //加载快照文件完成
}

func readLogfile(storepath string) []string {
	fpaths := make([]string, 0)

	fds := WalkDirectory(storepath)

	for _, v := range fds {
		fpaths = append(fpaths, v.Fpath)
	}
	return fpaths
}
