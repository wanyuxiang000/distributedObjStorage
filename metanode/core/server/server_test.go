/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package server

import (
	. "com.mgface.disobj/common"
	"flag"
	"fmt"
	"testing"
	"time"
)

func TestTCP(t *testing.T) {
	//元数据服务节点
	DefaultMatanodeAddr := "127.0.0.1:30000"
	server := flag.String("h", DefaultMatanodeAddr, "缓存服务器IP地址")
	op := flag.String("c", "get", "命令行操作,应该为get/set其中一种")
	key := flag.String("k", "test", "key")
	value := flag.String("v", "admin123", "value")
	flag.Parse()
	client := NewReconTCPClient(*server, 3)
	cmd := &Cmd{Name: *op, Key: *key, Value: *value}
	cmd.Run(client)
	if cmd.Error != nil {
		fmt.Println("error:", cmd.Error)
	} else {
		fmt.Println(cmd.Value)
		fmt.Println("等待2S准备退出连接")
		time.Sleep(time.Second * 2)
	}
}
