/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package nodeinfo

import (
	"sync"
)

// NodeInfo 节点的状态情况
type NodeInfo struct {
	//节点的标识:master,slave
	NodeFlag      string `json:"node_flag"`
	MutexNodeInfo sync.RWMutex
}

// DecideMaster 判断是否是master
func (nodeinfo *NodeInfo) DecideMaster() bool {
	flag := ""
	nodeinfo.MutexNodeInfo.RLock()
	flag = nodeinfo.NodeFlag
	nodeinfo.MutexNodeInfo.RUnlock()
	return flag == "master"
}

// SetMaster 设置节点为master
func (nodeinfo *NodeInfo) SetMaster() {
	nodeinfo.MutexNodeInfo.Lock()
	nodeinfo.NodeFlag = "master"
	nodeinfo.MutexNodeInfo.Unlock()
}

// GetNodeInfo 获得节点的状态
func (nodeinfo *NodeInfo) GetNodeInfo() string {
	flag := ""
	nodeinfo.MutexNodeInfo.RLock()
	flag = nodeinfo.NodeFlag
	nodeinfo.MutexNodeInfo.RUnlock()
	return flag
}
