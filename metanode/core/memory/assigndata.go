/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package memory

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
)

// AssignThis 设置自身数据
func (cache *MemoryStore) AssignThis(data interface{}) {

	//判断data是否是字符串类型
	var dx map[string]interface{}
	if err := json.Unmarshal(data.([]byte), &dx); err != nil {
		log.Warn("反序列化数据异常.")
	}

	if len(dx) > 0 {
		cache.Mutex.Lock()
		defer cache.Mutex.Unlock()
		for k, v := range dx {
			cache.Datas[k] = v
		}
	}
}
