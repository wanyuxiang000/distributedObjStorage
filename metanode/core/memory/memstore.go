/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package memory

import (
	. "com.mgface.disobj/common"
	"sync"
)

// MemoryStore 实现Store接口
type MemoryStore struct {
	//存储的路径
	StorePath string `json:"store_path"`
	//互斥锁
	Mutex sync.RWMutex
	//存储数据的map
	Datas map[string]interface{} `json:"datas"`
	//是否开启后台定期清理数据
	EnableClean bool `json:"enable_clean"`
	//接收到的消息
	Msgs chan RecMsg `json:"msgs"`
	//快照生成完成的标志
	FishedSnapshot chan bool `json:"fished_snapshot"`
	//加载快照文件标识
	LoadingSnapshot bool `json:"loading_snapshot"`
	//缓冲的消息
	BuffMsgs chan RecMsg `json:"buff_msgs"`
	//缓冲的信号量
	BuffSemaphore chan bool `json:"buff_semaphore"`
}
