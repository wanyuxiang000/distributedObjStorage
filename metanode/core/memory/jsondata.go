/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package memory

import "encoding/json"

// ThisToJson 返回自身的数据
func (cache *MemoryStore) ThisToJson() string {
	cache.Mutex.Lock()
	defer cache.Mutex.Unlock()
	//只传输这些心跳数据给其他mtanode数据
	dataTransfer := make(map[string]interface{}, 3)
	if v, ok := cache.Datas["dataNodes"]; ok {
		dataTransfer["dataNodes"] = v
	}
	if v, ok := cache.Datas["apiNodes"]; ok {
		dataTransfer["apiNodes"] = v
	}
	if v, ok := cache.Datas["metaNodes"]; ok {
		dataTransfer["metaNodes"] = v
	}
	data, _ := json.Marshal(dataTransfer)
	return string(data)
}
