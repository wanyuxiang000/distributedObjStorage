/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package memory

import (
	. "com.mgface.disobj/common"
	. "com.mgface.disobj/metanode/core/nodeinfo"
	"time"
)

// ExpireData 过期内存数据
func (cache *MemoryStore) ExpireData(nodeinfo *NodeInfo, intervalCleanMem int) {
	if cache.EnableClean {
		for {
			time.Sleep(time.Duration(intervalCleanMem) * time.Second)
			//判断是否是master
			master := nodeinfo.DecideMaster()
			cache.Mutex.Lock()
			//假如当前节点不是master，把自身变成slave，不主动启动心跳检测过期处理
			//主要是为了让datanaode只向master节点汇报心跳，减少tcp交互，然后由master把内存存储的meta数据同步给其他slave
			//APINode服务可以接入slave，因为slave有完整从master同步过来的内存数据，可以达到提高多个matanoade的使用效率
			if master {
				for key, mvalues := range cache.Datas {
					//对心跳数据进行过期处理
					if key == "dataNodes" || key == "apiNodes" || key == "metaNodes" {
						legal := make([]MetaValue, 0)
						if forceTxData, ok := mvalues.([]MetaValue); ok {
							for _, value := range forceTxData {
								if time.Now().Before(value.Created.Add(time.Duration(intervalCleanMem) * time.Second)) {
									legal = append(legal, value)
								}
							}
						}
						cache.Datas[key] = legal
					}
				}
			}
			cache.Mutex.Unlock()
		}
	}
}
