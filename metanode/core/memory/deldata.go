/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package memory

import . "com.mgface.disobj/common"

func (cache *MemoryStore) Del(key string, value []byte) error {
	cache.Mutex.Lock()
	defer cache.Mutex.Unlock()
	mvalues, exit := cache.Datas[key]
	if key == "search" {
		if exit {
			forceTxData := mvalues.([]MetaValue)
			//假如数据长度为1，说明是单节点数据交互
			if len(forceTxData) == 1 {
				delete(cache.Datas, key)
				return nil
			}
			data := make([]MetaValue, 0)
			for _, val := range forceTxData {
				if val.RealNodeValue != string(value) {
					data = append(data, val)
				}
			}
			cache.Datas[key] = data
		}
	} else {
		delete(cache.Datas, key)
	}
	return nil
}
