/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package memory

import (
	"encoding/json"
	"fmt"
	"time"
)

// Show 显示内存数据信息到控制台
func (cache *MemoryStore) Show() string {
	for {
		cache.Mutex.RLock()
		data, err := json.MarshalIndent(cache.Datas, "", "\t")
		//data, err := json.Marshal(cache.Datas)
		cache.Mutex.RUnlock()
		if err != nil {
			return "发生错误信息:" + err.Error()
		}
		fmt.Println("内存数据:", string(data))
		time.Sleep(5 * time.Second)
	}
}
