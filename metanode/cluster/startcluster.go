/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package cluster

import (
	"com.mgface.disobj/metanode/core/handler"
)

// StartGossipCluster 启动gossip cluster集群
func StartGossipCluster(nodeAddr, cluster, gossipAddr, podNamespace, serviceName string, serv *handler.Server, startflag chan bool) {
	//1.加入集群
	nodename, broadcasts, list := joinGossipCluster(nodeAddr, cluster, gossipAddr, podNamespace, serviceName, serv)

	//2.显示集群状态
	showMemberist(list)

	//3.master发送集群消息
	go sendMsg2Cluster(nodename, serv, broadcasts, list, startflag)
}
