/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package cluster

import (
	"github.com/hashicorp/memberlist"
	"strconv"
	"strings"
)

// WrapMemberlistNodes 节点进行排序
type WrapMemberlistNodes []*memberlist.Node

func (s WrapMemberlistNodes) Len() int {
	return len(s)
}

func (s WrapMemberlistNodes) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s WrapMemberlistNodes) Less(i, j int) bool {
	nodenamei := strings.Split(s[i].Name, "-")
	nodei, _ := strconv.ParseInt(nodenamei[0], 10, 64)
	nodenamej := strings.Split(s[j].Name, "-")
	nodej, _ := strconv.ParseInt(nodenamej[0], 10, 64)
	return nodei < nodej
}
