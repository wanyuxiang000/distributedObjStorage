/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package common

import "os"

type FileDesc struct {
	Filename string      `json:"filename"` //文件名称
	Fpath    string      `json:"fpath"`    //文件路径
	FD       os.FileInfo `json:"-"`        //文件内容
}

type FileDescs []FileDesc

// Len 数据分片切片长度
func (s FileDescs) Len() int {
	return len(s)
}

// Swap 数据分片的数据交换
func (s FileDescs) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Less 数据分片的比较
func (s FileDescs) Less(i, j int) bool {
	//降序
	return s[i].Filename > s[j].Filename
}
