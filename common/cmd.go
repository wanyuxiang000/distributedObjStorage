/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package common

import (
	"errors"
	"fmt"
)

// Cmd 执行的命令
type Cmd struct {
	//操作命令
	Name string `json:"name"`
	//操作的key
	Key string `json:"key"`
	//操作的value
	Value string `json:"value"`
	//异常信息
	Error error `json:"error"`
}

// Run 执行客户端请求命令
func (cmd *Cmd) Run(client *TcpClient) {
	if cmd.Name == "get" {
		_, err := client.sendGet(cmd.Key, cmd.Value)
		if err != nil {
			cmd.Error = err
		} else {
			cmd.Value, cmd.Error = client.recvResponse()
		}
		return
	}
	if cmd.Name == "set" {
		_, err := client.sendSet(cmd.Key, cmd.Value)
		if err != nil {
			cmd.Error = err
		} else {
			_, cmd.Error = client.recvResponse()
		}
		return
	}
	if cmd.Name == "del" {
		_, err := client.sendDel(cmd.Key, cmd.Value)
		if err != nil {
			cmd.Error = err
		} else {
			_, cmd.Error = client.recvResponse()
		}
		return
	}
	cmd.Error = errors.New(fmt.Sprintf("未知的命令: %s,仅支持set,get,del操作.", cmd.Name))
}
