/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package common

import (
	"bufio"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

// TcpClient 请求客户端
type TcpClient struct {
	net.Conn
	reader *bufio.Reader
}

// 缓存连接，不做无谓的频繁创建连接
var cacheClient sync.Map

// NewTCPClient 构建请求客户端
func NewTCPClient(server string) *TcpClient {
	c, e := net.Dial("tcp", server)
	if e != nil {
		return nil
	}
	r := bufio.NewReader(c)
	return &TcpClient{c, r}
}

// NewReCallFuncTCPClient 构建调用函数动态获取服务端IP地址的客户端
func NewReCallFuncTCPClient(server func() string, maxFailCount int) *TcpClient {
	failCount := 0
retry:
	//重连的时候方法连接超时时间
	//BUG(billy) windows10 go1.15版本，设置读取超时时间无效，不管设置多长，最大2秒就返回了
	serverIp := server()
	log.Debug("serverIp:::::", serverIp)
	c, e := net.DialTimeout("tcp", serverIp, 2*time.Second)
	if e != nil {
		failCount++
		//重连maxFailCount次，如果还不成功，那么直接返回nil
		if failCount > maxFailCount {
			return nil
		}
		goto retry
	} else {
		if failCount > 0 {
			log.Info(fmt.Sprintf("重连成功，重连的IP地址为:%s", serverIp))
		}
	}
	r := bufio.NewReader(c)
	return &TcpClient{c, r}
}

// NewReconTCPClient 构建可重连请求客户端
func NewReconTCPClient(server string, maxFailCount int) *TcpClient {
	failCount := 0
retry:
	//重连的时候方法连接超时时间
	//times := 4
	//BUG(billy) windows10 go1.15版本，设置读取超时时间无效，不管设置多长，最大2秒就返回了
	c, e := net.DialTimeout("tcp", server, 2*time.Second)
	if e != nil {
		failCount++
		//重连maxFailCount次，如果还不成功，那么直接返回nil
		if failCount > maxFailCount {
			return nil
		}
		goto retry
	}
	r := bufio.NewReader(c)
	return &TcpClient{c, r}
}

func (client *TcpClient) sendDel(key, value string) (n int, err error) {
	klen := len(key)
	vlen := len(value)
	return client.Write([]byte(fmt.Sprintf("D%d %d %s%s", klen, vlen, key, value)))
}

func (client *TcpClient) sendGet(key, value string) (n int, err error) {
	klen := len(key)
	vlen := len(value)
	return client.Write([]byte(fmt.Sprintf("G%d %d %s%s", klen, vlen, key, value)))
}

func (client *TcpClient) sendSet(key, value string) (n int, err error) {
	klen := len(key)
	vlen := len(value)
	return client.Write([]byte(fmt.Sprintf("S%d %d %s%s", klen, vlen, key, value)))
}

// 接受客户端响应数据
func (client *TcpClient) recvResponse() (string, error) {
	tmp, e := client.reader.ReadString(' ')
	if e != nil {
		return "", e
	}
	vlen, _ := strconv.Atoi(strings.TrimSpace(tmp))
	if vlen == 0 {
		return "", nil
	}
	if vlen < 0 {
		err := make([]byte, -vlen)
		_, e := io.ReadFull(client.reader, err)
		if e != nil {
			return "", e
		}
		return "", errors.New(string(err))
	}
	value := make([]byte, vlen)
	_, e = io.ReadFull(client.reader, value)
	if e != nil {
		return "", e
	}
	return string(value), nil
}
