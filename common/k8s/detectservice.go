/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package k8s

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	v1 "k8s.io/api/core/v1"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func client() *http.Client {

	pemData, _ := os.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")
	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(pemData)

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{RootCAs: pool},
	}
	return &http.Client{Transport: tr}
}

var k8sToken = "/var/run/secrets/kubernetes.io/serviceaccount/token"

func DetectMetaService(port, namespace, serviceName string) []string {
	//------------------k8s operations----------------
	log.Debug("暂停5~10s,等待k8s进行更新数据......")
	time.Sleep(time.Duration(5+rand.Intn(5)) * time.Second)
	var request *http.Request

	search := fmt.Sprintf("https://kubernetes.default.svc/api/v1/namespaces/%s/endpoints", namespace)

	request, _ = http.NewRequest("GET", search, nil)
	var token string

	fileInfo, _ := os.Lstat(k8sToken)
	if fileInfo.Mode()&os.ModeSymlink != 0 {
		realPath, _ := filepath.EvalSymlinks(k8sToken)
		tokenBytes, _ := os.ReadFile(realPath)
		token = string(tokenBytes)
	}

	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	resp, _ := client().Do(request)
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	fmt.Println("获取service值为:", string(body))
	//解析断点地址
	var endpoints v1.EndpointsList
	json.Unmarshal(body, &endpoints)

	metaNodes := make([]string, 0)
	for _, v := range endpoints.Items {
		if strings.HasPrefix(serviceName, v.Name) {
			for _, v1 := range v.Subsets {
				for _, v2 := range v1.Addresses {
					metaNodes = append(metaNodes, fmt.Sprintf("%s:%s", v2.IP, port))
				}
			}
		}
	}
	return metaNodes
}
