/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package k8s

type EnvConfig struct {
	//pod命名空间
	Pns string `env:"POD_NAMESPACE"`
	//node地址
	Na string `env:"NODE_ADDR"`
	//node端口
	Napt string `env:"NODE_PORT",default:"3000"`
	//集群地址
	Ca string `env:"CLUSTER_ADDRS"`
	//集群地址端口
	Capt string `env:"CLUSTER_ADDRS_PORT"` //`default:""`
	//真是数据存储路径
	DataPath string `env:"DATA_PATH"`
	//元数据存储路径
	MataDataPath string `env:"META_DATA_PATH"`
	//服务名称
	Svc   string `env:"SERVICE_NAME"`
	Gnapt string `env:"GOSSIP_NODE_PORT"` //`default:""`
	//数据分片大小数量
	Ds string `env:"DS"`
	//奇偶校验数量
	Ps string `env:"PS"`
	//Hosts   []string `slice_sep:","`
}
