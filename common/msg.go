/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package common

// ReturnMsg 返回客户端信息
type ReturnMsg struct {
	Msg  string `json:"msg"`  //返回信息
	Flag bool   `json:"flag"` //返回的错误标识
}

// RecMsg 服务器段接收到的消息
type RecMsg struct {
	Key string      `json:"key"`
	Val interface{} `json:"val"`
}
