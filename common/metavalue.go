/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package common

import (
	"encoding/json"
	"strings"
	"time"
)

type MetaValue struct {
	RealNodeValue string    `json:"real_node_value"` //node_ip值
	Created       time.Time `json:"created"`         //缓存创建时间
}

type WrapMetaValues []MetaValue

func (s WrapMetaValues) Len() int {
	return len(s)
}

func (s WrapMetaValues) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s WrapMetaValues) Less(i, j int) bool {
	inode := strings.Split(s[i].RealNodeValue, "-")
	var ivar int
	if inode[1] == "master" {
		ivar = 99
	} else {
		ivar = 100
	}
	jnode := strings.Split(s[j].RealNodeValue, "-")
	var jvar int
	if jnode[1] == "master" {
		jvar = 99
	} else {
		jvar = 100
	}
	return ivar < jvar
}

// 定义一个新的结构体用来序列化标识
type tempMetaValue struct {
	RealNodeValue string `json:"real_node_value"`
	Created       string `json:"created"`
}

// MarshalJSON 实现它的json序列化方法
func (v MetaValue) MarshalJSON() ([]byte, error) {
	// 定义一个新的结构体
	tempMetaValue := &tempMetaValue{
		RealNodeValue: v.RealNodeValue,
		Created:       v.Created.Format("2006-01-02 15:04:05"),
	}
	return json.Marshal(tempMetaValue)
}

// UnmarshalJSON 实现它的json反序列化方法
func (v *MetaValue) UnmarshalJSON(data []byte) error {
	var tmp tempMetaValue
	json.Unmarshal(data, &tmp)

	v.RealNodeValue = tmp.RealNodeValue
	local, err := time.ParseInLocation("2006-01-02 15:04:05", tmp.Created, time.Local)
	v.Created = local
	//v = &MetaValue{tmp.RealNodeValue, local}
	return err
}
