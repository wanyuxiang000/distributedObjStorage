#!/bin/bash

#
# Copyright 2021-2023 mgface(mgface2022@outlook.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and limitations under the License.
#

if [ "$1" = "choas" ]; then
  #制造混乱，看看其他服务的反应程度
  kubectl delete deploy/nginx -n mgface-disobjstorage
  echo "1)删除API数据服务......"
  kubectl delete deploy/apinode -n mgface-disobjstorage
  echo "2)删除数据节点服务......"
  kubectl delete deploy/datanode -n mgface-disobjstorage
  echo "3)删除元数据节点服务......"
  kubectl delete deploy/metanode -n mgface-disobjstorage
  echo "4)删除namespace......"
  kubectl delete namespace mgface-disobjstorage
else
  echo "1)删除nginx对外提供服务"
  kubectl delete deploy/nginx -n mgface-disobjstorage
  echo "2)删除API数据服务......"
  kubectl delete deploy/apinode -n mgface-disobjstorage
  echo "3)删除数据节点服务......"
  kubectl delete deploy/datanode -n mgface-disobjstorage
  echo "4)删除元数据节点服务......"
  kubectl delete deploy/metanode -n mgface-disobjstorage
  echo "5)删除namespace......"
  kubectl delete namespace mgface-disobjstorage
  echo "end.删除完毕."
fi
