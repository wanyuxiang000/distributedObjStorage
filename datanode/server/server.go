/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package server

import (
	. "com.mgface.disobj/common"
	. "com.mgface.disobj/datanode/api"
	"com.mgface.disobj/datanode/datarepair"
	. "com.mgface.disobj/datanode/hander"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func StartServer(nodeAddr, metaNodeAddr, podNamespace string) {
	log.Info("启动数据节点...")
	log.Info(fmt.Sprintf("节点地址:%s", nodeAddr))
	log.Info(fmt.Sprintf("元数据服务节点地址:%s", metaNodeAddr))
	//后台数据修复
	go datarepair.Repair()

	//创建启动标志,一个用来启动发送心跳服务,一个用来更新数据节点数据

	flag := make(chan bool)

	//动态更新metaNodeAddr
	go RefreshMetaNodeAddr(metaNodeAddr, podNamespace, flag)
	//心跳汇报
	go StartHeartbeat(nodeAddr, flag)

	http.HandleFunc("/objects/", ApiHandler)

	ServeAndGracefulExit(nodeAddr)
}
