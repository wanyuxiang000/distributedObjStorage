/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package datarepair

import (
	. "com.mgface.disobj/datanode/api"
	"crypto/sha256"
	"encoding/hex"
	"os"
	"path/filepath"
	"time"
)

// 存储文件的描述信息
type fileDesc struct {
	filename string `json:"filename"`
	filehash string `json:"filehash"`
}

func Repair() {
	for {
		filedescs := make([]fileDesc, 0)

		//也可以直接使用
		//files, _ := filepath.Glob(os.Getenv("STORAGE_ROOT") + "/objects")
		//for i := range files {
		//	hash := strings.Split(filepath.Base(files[i]), ".")[0]
		//	verify(hash)
		//}
		filepath.Walk(GetRollingStorePath(), func(path string, f os.FileInfo, err error) error {
			if f == nil {
				return err
			}
			if f.IsDir() {
				return nil
			}
			readdata, _ := os.ReadFile(path)
			//算出文件的hash值
			hash := sha256.New()
			hash.Write(readdata)
			hashInBytes := hash.Sum(nil)
			sharedHashValue := hex.EncodeToString(hashInBytes)
			desc := fileDesc{
				filename: GetNodeAddr() + string(os.PathSeparator) + f.Name(),
				filehash: sharedHashValue,
			}
			filedescs = append(filedescs, desc)
			//log.Debug("file=>", path, "f=>", f.Name())
			return nil
		})

		//todo 准备查询filecrc数据，判断我们读取的文件是否hash码值变更了，如果变了或者不存在，说明数据已经丢失了，需要重塑出来
		//todo 先把批量数据发送到mgfaceMQ里面的filecrc进行校验，如果校验失败，那么在mgfaceMQ里面数据还原，并对需要还原的数据块
		//todo 重新插入
		time.Sleep(5 * time.Second)
	}

}
