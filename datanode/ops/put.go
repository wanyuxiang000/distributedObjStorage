/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package ops

import (
	. "com.mgface.disobj/common"
	. "com.mgface.disobj/datanode/api"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

func Put(writer http.ResponseWriter, req *http.Request) {
	url := req.URL.EscapedPath()
	//获取对象名称
	objName := strings.Split(url, "/")[2]
	//修改底层数据名称
	objRealName := objName + "-" + strconv.FormatInt(time.Now().UnixNano(), 10)
	//例如："10.1.2.207:5000/sdp/20210207-10.1.2.207-5000/abc17787-1612644719934396484"
	filename := GetRollingStorePath() + string(os.PathSeparator) + objRealName
	realURL := ""
	if runtime.GOOS == "windows" {
		realURL = GetNodeAddr() + string(os.PathSeparator) + filename
	} else {
		realURL = GetNodeAddr() + filename
	}

	//对传输的数据进行hash运算
	var data []byte
	if req.Body != nil {
		data, _ = io.ReadAll(req.Body)
	}

	file, err := os.Create(filename)
	if err != nil {
		log.Debug(err)
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))
		return
	}
	defer file.Close()
	file.Write(data)
	//强制把缓存页刷到硬盘
	file.Sync()

	index, _ := strconv.Atoi(req.Header.Get("index"))
	hashValue := req.Header.Get("hash")
	hash := sha256.New()
	hash.Write(data)
	hashInBytes := hash.Sum(nil)
	sharedHashValue := hex.EncodeToString(hashInBytes)
	//记录文件的CRC
	status, e := buildFileCRC(hashValue, sharedHashValue, realURL, index)
	writer.WriteHeader(status)
	if e != nil {
		writer.Write([]byte(e.Error()))
	}
}

// 构建文件的CRC文件
//
// hashValue 完整文件的hash值
//
// sharedValue 分片文件的hash值
//
// realURL 分片存放路径
//
// index 分片索引号
func buildFileCRC(hashValue, sharedHashValue, realURL string, index int) (int, error) {
	//做文件去重插入数据
	crc := &FileCRC{
		Data: make(map[string][]SharedData),
	}

	sharedData := SharedData{
		SharedFileUrlLocate: realURL,
		SharedFileHash:      sharedHashValue,
		SharedIndex:         index,
	}
	sd := make([]SharedData, 0)
	sd = append(sd, sharedData)
	crc.Data[hashValue] = sd
	data, _ := json.Marshal(crc)
	cmd := &Cmd{Name: "set", Key: "filecrc", Value: string(data)}

	client := NewReCallFuncTCPClient(GetMetaNodeAddr, 3)
	if client == nil {
		tips := fmt.Sprintf("获取动态metanode值失败.")
		log.Warn(tips)
		return http.StatusInternalServerError, errors.New(tips)
	}

	cmd.Run(client)
	if cmd.Error != nil {
		return http.StatusInternalServerError, cmd.Error
	}
	return http.StatusOK, nil
}
