/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package ops

import (
	. "com.mgface.disobj/datanode/api"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"strings"
)

func Get(writer http.ResponseWriter, req *http.Request) {
	url := req.URL.EscapedPath()
	rollingStorePath := strings.Split(url, "/")[2]
	objName := strings.Split(url, "/")[3]
	realObjName := fmt.Sprintf("%s/%s", rollingStorePath, objName)
	//定位实际的文件位置
	filename := GetStorePath() + string(os.PathSeparator) + realObjName
	file, err := os.Open(filename)
	if err != nil {
		log.Debug(err)
		writer.WriteHeader(http.StatusNotFound)
		writer.Write([]byte(err.Error()))
		return
	}

	defer file.Close()
	_, err = io.Copy(writer, file)
	if err != nil {
		writer.WriteHeader(http.StatusOK)
	}
}
