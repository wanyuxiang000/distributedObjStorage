/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import "sync"

// 全局锁
var dataNodeMutex sync.RWMutex

// 因为metaNodeAddr是全局变量.为了保证可见性,使用时需要使用apiMutex全局锁
var metaNodeAddr string

// GetMetaNodeAddr 获取metaNode地址
func GetMetaNodeAddr() string {
	dataNodeMutex.RLock()
	current := metaNodeAddr
	dataNodeMutex.RUnlock()
	return current

}

// SetMetaNodeAddr 设置MetaNode地址
func SetMetaNodeAddr(nodeAddr string) {
	dataNodeMutex.Lock()
	metaNodeAddr = nodeAddr
	dataNodeMutex.Unlock()
}
