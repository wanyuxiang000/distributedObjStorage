/*
 * Copyright 2021-2023 mgface(mgface2022@outlook.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and limitations under the License.
 */

package api

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"strings"
	"time"
)

// 文件存储路径
var storePath string

func GetStorePath() string {
	return storePath
}

func setStorePath(_storePath string) {
	storePath = _storePath
}

// 每天滚动生成的文件存储路径
var rollingStorePath string

// GetRollingStorePath 获取滚动日期存储的本地存储路径
func GetRollingStorePath() string {
	return rollingStorePath
}

func setRollingStorePath(_rollingStorePath string) {
	rollingStorePath = _rollingStorePath
}

// 当前节点的地址
var nodeAddr string

func GetNodeAddr() string {
	return nodeAddr
}

func setNodeAddr(ndAddr string) {
	nodeAddr = ndAddr
}

func InitParams(datapath, nodeAddr string) {
	setStorePath(datapath)

	datePath := time.Now().Format("20060102")
	nodePath := strings.Replace(nodeAddr, ":", "-", -1)
	//每天生成文件存储的目录
	//二级目录不能随机大小字母，要按照日期来生成
	localStorePath := fmt.Sprintf("%s%s%s-%s", datapath, string(os.PathSeparator), datePath, nodePath)
	setRollingStorePath(localStorePath)
	log.Debug("文件存储的目录:", localStorePath)
	//如果目录不存在，这新建该目录，授予目录权限为777

	//目录不存在则创建
	if _, err := os.Stat(localStorePath); os.IsNotExist(err) {
		os.MkdirAll(localStorePath, os.ModePerm)
	}
	setNodeAddr(nodeAddr)
}
